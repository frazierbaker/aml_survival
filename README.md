# Neural Network Regression for Acute Myeloid Leukemia Survival
CS 7097: Introduction to Functional Genomics | Q Paper<br>
Frazier N. Baker<sup>1,2,\*</sup><br>
<sub>1 Department of Electrical Engineering and Computer Science, University of Cincinnati, 2901 Woodside Drive 
Cincinnati, Ohio, USA 45221</sub><br>
<sub>2 Center for Autoimmune Genomics and Etiology, Cincinnati Children's Hospital Medical Center, 3333 Burnet Avenue, 
Cincinnati, Ohio, USA 45229</sub><br>
<sub>\*To whom correspondence should be addressed</sub>

## Introduction
Gene expression profiles of tumours have been shown to convey information about clinical outcomes for acute myeloid leukemia (AML) patients<sup>1,2,3</sup>.  Subtyping AML often requires examining cytogenetic features and mutations; however, mRNA expression profiles can also be useful in subtyping AML<sup>1,2,3</sup>. Some subtyping strategies are correlated with overall survival<sup>1,3</sup>.  Understanding overall survival trends, however, requires more information than a simple subtyping classification can offer.

Capturing subtle nuances in the expression profiles of the cancer cells may offer a more refined prognostic view.  In addition, other clinical factors, such as age and severity of disease at time of diagnosis, are suggested to contribute to survival<sup>3</sup>.  Neural networks have been used to implement regressions based on large, high dimensional datasets<sup>4</sup>.  Here, a neural network regression model is proposed to estimate survival of a patient based on clinical observations paired with mRNA expression profiles of their cancer.

## Question
Given the mRNA expression profile of a cancer of a patient and other clinical observations, can a neural network-based regression model be trained to estimate overall survival time?

## Methods
### Expression Data
AML samples and clinical data were obtained from the Cancer Genome Atlas (TCGA) AML study through cBioPortal<sup>5,6,7</sup>. Z-score normalized mRNA expression data was taken directly from the cBioPortal data files.  This data file contained information contained 1929 of the 2000 genes identified by Baker et al., who did their own normalization from the median expression values provided in the same data<sup>1</sup> These 1929 genes were used as RNA expression input for each of the 173 samples with RNA expression data in the dataset.  This dataset was divided randomly into three subsets: training (n=96), validation (n=25), and test (n=52).
### Clinical Data
Paired data about the patients from whom these samples were taken was also available. Only some of this data was selected as input features to avoid relying on specific subtyping methods to be performed prior to regression.  Four features were extracted: diagnosis age, sex, peripheral blood blast percentage (PBB%), and white blood cell count (WBC).

Sex was quantified as a binary feature (1=female, 0=male).  Diagnosis age was provided in whole years.  PBB% was provided as an integer between 0 and 100.  When missing, PBB% was assigned to 0.  PBB% values were then divided by 100 to put them between 0 and 1.  WBC was provided as a number with precision to one decimal place in units of 109 cells/L.  WBC was z-score normalized, as its original values varied from 0 to over 298.4x109 cells/L.

Survival, the target of the regression, was also provided in the clinical data.  It was provided in months, with 1 digit of precision after the decimal place.  After attempting to train a regression on the data as it was given and analyzing the outliers who survive much longer than two years, the data was capped at 24 months, with those surviving more than 24 months being assigned the value of 24 for overall survival.  This includes all but 5 of those whose survival status was listed as “living” in the clinical data.  These 5 individuals were assumed  to be alive at 24 months for the sake of this regression.
### Network Design
The regression model was designed as a neural network using the Keras<sup>8</sup> framework with a Tensorflow<sup>9</sup> backend. SciKit-Learn<sup>10</sup>, Pandas<sup>11</sup>, and NumPy<sup>12</sup> were used in preprocessing data and evaluating results.  The design of the network is split into two parts: the expression subnetwork and combined model including both clinical and gene expression features.  Due to the lack of balance in features (1929 genes vs. 4 clinical features), the gene expression subnetwork was trained to function as a regression to predict survival first.  Then, the 4 clinical features were combined with the output of this network and fed into a final network that was trained for the same regression.

Different network depths (N_hidden in {0, 1,2}),  learning rate parameterss (lr(0.0001,0.01)), layer widths (N_nodes in {1,4,8,16,32,64,128,256,1024,2048,4096}), regularizers (L1, L2), and activation functions (RELU, Sigmoid) were tried in hyperparameter tuning.  Hyperparameters were tuned with respect to the value of the validation loss function, which was the mean squared error.  Additionally, adding a dropout layer was explored.  RELU and linear activation functions were explored for the output nodes.  The Adam optimization algorithm was used to train the model.  The final model design is pictured in Figure 1. The expression model was trained with a learning rate parameter of 0.005 and the combined model was trained with a learning rate parameter of 0.01.  These models were trained on a Google Colaboratory (http://colaboratory.google.com) notebook using the TPU hardware acceleration option.

![Figure 1](https://gitlab.com/frazierbaker/aml_survival/raw/master/design.png "Figure 1")

## Results
### Expression Model
Trained on expression data alone, the model was able to converge to a mean squared error of 106.7635 on the validation set after 3000 epochs of training.  Once finalized, this model alone was run on the test set.  The results are plotted in Figure 2.  It achieved a mean absolute error of **7.11** months and a mean squared error of **80.28** on the test set.  The predicted values correlated to the actual values with a Spearman score of **0.5273**  and a p-value of **5.9082x10-5**.  The model is fairly pessimistic with a mean error of **-3.091**.

![Figure 2](https://gitlab.com/frazierbaker/aml_survival/raw/master/expression.png "Figure 3")

### Combined Model
The model which considered the output of the first model and the four clinical features achieved a mean squared error of 89.1393 on the validation set after 1000 iterations.  Once finalized, this model was run on the test set.  The results are plotted in Figure 3.  It achieved a mean absolute error of **6.99** months and a mean squared error of **67.55** on the test set.  The predicted values correlated with the actual values with a Spearman score of **0.5450** and a p-value of **2.9441x10-5**.  There is a shift upward in the predicted values, making the combined model more optimistic  with a mean error of **-2.39**.

![Figure 3](https://gitlab.com/frazierbaker/aml_survival/raw/master/combined.png "Figure 3")

## Discussion
### Performance
Performance increased when incorporating the clinical information, but not by a huge amount.  This could be because some of the clinical information may be inferable from the RNA expression.  Additionally, the amount of data available in this study is insufficient for deep learning applications.  With access to more data, this regression has the potential to improve.  The other data sources available require formal requests for access to controlled data, which is not feasible for a graduate student alone.
While this model is far from perfect, it is important in every machine learning project to consider the baseline against which we compare.  Estimating survival is a challenging problem, and one study has shown that, in terminal illnesses, 80% of the time human experts tend to be inaccurate in their estimates by more than 33% of survival time<sup>13</sup>.   In the combined model, 22 of 52 test points (**42.3%**) fall within that range; however, this is not accounting for the fact that our model only goes to 24 months.  Physicians tend to be optimistic when offering prognostic estimates<sup>13</sup>; this estimate’s pessimism is a shift from common practice.
### Ethics
This regression attempts to estimate survival over a two year period for individuals with a life-threatening disease.  While an interesting concept to study in theory, practical applications have ethical considerations attached.  Families and patients often desire to have an accurate prognosis when deciding on treatment; however, insurance companies and other financial entities may use this information when making decisions on funding treatment.  Before implementing any survival regression model clinically, one needs to carefully explore and understand the ethical implications.
## Conclusion
A survival regression model based on genetic and clinical data is feasible and may one day offer better prognostic estimates; however, more data are necessary to better harness the power of deep learning in this approach.
## References
1. Baker FN, Ghaderian M, Karve A, Ruwe A, and Wissel B. Data Landscape of Acute Myeloid Leukemia. unpublished. 2018.
1. Tyner JW, et al. Functional genomic landscape of acute myeloid leukaemia. Nature. 2018;562(7728):526–531. doi:10.1038/s41586-018-0623-z
1. Döhner, Hartmut, et al. Diagnosis and management of acute myeloid leukemia in adults: recommendations from an international expert panel, on behalf of the European LeukemiaNet. Blood. 2010;115(3):453-474. doi:10.1182/blood-2009-07-235358
1. Chollet F. Deep learning with Python. Manning; 2017.
1. The Cancer Genome Atlas Research Network. Genomic and Epigenomic Landscapes of Adult De Novo Acute Myeloid Leukemia. New England Journal of Medicine. 2013;368(22):2059–2074. doi:10.1056/nejmoa1301689
1. Cerami E, et al. The cBio Cancer Genomics Portal: An Open Platform for Exploring Multidimensional Cancer Genomics Data: Figure 1. Cancer Discovery. 2012;2(5):401–404. doi:10.1158/2159-8290.cd-12-0095
1. Gao J, et al. Integrative Analysis of Complex Cancer Genomics and Clinical Profiles Using the cBioPortal. Science Signaling. 2013;6(269). doi:10.1126/scisignal.2004088
1. Chollet F. Keras. 2015. http://keras.io
1. Abadi M et al. Tensorflow: a system for large-scale machine learning. OSDI. 2016;16:265-283.
1. Pedregosa F, et al. Scikit-learn: Machine learning in Python. Journal of machine learning research. 2011;12(Oct):2825-2830.
1. Oliphant TE. A guide to NumPy. Trelgol Publishing; 2006.
1. McKinney W. Data Structures for Statistical Computing in Python. Proceedings of the 9th Python in Science Conference. 2010:51-56.
1. Christakis NA, Lamont EB. Extent and determinants of error in physicians' prognoses in terminally ill patients: prospective cohort study. Western Journal of Medicine 2000;172(5):310-3.

